
extern crate csv;
extern crate rustc_serialize;
extern crate time;
use std::env;
use std::process;
use time::strptime;
// use std::io;
use std::f64;
use std::cmp;
use std::collections::HashMap;


const SKIP_TIME: i64 = 10;

const MIN_DIST: f64 = 1.5;


#[derive(RustcDecodable, Clone)]
struct Record {
    long: Option<f64>,
    lat: Option<f64>,
    id: u32,
}

#[derive(Clone, Debug)]
struct Coord {
   long: f64,
   lat: f64,
   time: i64,
}

impl Coord{
   fn dist(&self, other: &Coord) -> f64 {
      // result in meter
      let earth_radius:f64 = 6371009f64;
      let rad_conv = f64::consts::PI/180.0;
      let lat_dif = (self.lat - other.lat)*rad_conv;
      let mean_lat = (self.lat + other.lat) * rad_conv/2.0;
      let long_dif = (self.long - other.long)*rad_conv;
      earth_radius * ( lat_dif.powi(2) + (mean_lat.cos()*long_dif).powi(2)).sqrt()
   }
}

#[derive(Clone)]
struct Timespan {
   beg: i64,
   end: i64,
}

impl Timespan{
   fn intersect(&self, other: &Timespan) -> i64 {
       cmp::min(self.end, other.end) - cmp::max(self.beg,other.beg)
   }
}

fn proximity(locations: &HashMap<u32, Coord>) -> Vec<(u32,u32)>{
   let mut res:Vec<(u32, u32)> = Vec::new();
   let mut it = locations.iter();

   loop {
       match it.next() {
           None => break,
           Some(el1) => {
             let mut it2 = it.clone();
             loop{
                match it2.next() {
                   Some(el2) => {
                                 let (k1, c1) = el1;
                                 let (k2, c2) = el2;
                                 if c1.dist(c2) < MIN_DIST {
                                    res.push((*k1,*k2));
                                 }
                                 // println!("Distance entre {:?} et {:?} = {:2}. {:?} {:?}",k1,k2,c1.dist(c2),c1,c2)
                                 },
                   None => break,
                }
             }
           },
       }
   }
   res
}

fn add_activation(time: i64, activations:&Vec<(u32,u32)>, cur_state: &mut HashMap<(u32,u32), Timespan>){
   let cur_span = Timespan{beg:time, end:time+SKIP_TIME};
   for pair in activations {
      let span:& mut Timespan = cur_state.entry(*pair).or_insert(cur_span.clone());
      if span.intersect(&cur_span) >=0 {
         assert!(cur_span.beg>=span.beg,"The new activation start before the old one...");
         span.end = cmp::max(cur_span.end, span.end);
      }else{
         println!("{} {} {} {}",span.beg, pair.0, pair.1,span.end-span.beg);
         span.beg = time;
         span.end = time+SKIP_TIME;
      }
   }
}

fn clean_stream(cur_state: &mut HashMap<(u32,u32), Timespan>){
   for (nodes, span) in cur_state {
      println!("{} {} {} {}",span.beg, nodes.0, nodes.1,span.end-span.beg);
   }
}

fn main() {
    let args: Vec<_> = env::args().collect();
   //  let proximity = 1; // 1 meter to make a contact
   //  let skip = 5; // second of inactivity before breaking a contact
    if args.len()  < 2 {
      println!("Need at least one argument");
      process::exit(0);
   }
    let ref path = args[1];
    let mut reader = csv::Reader::from_file(path).unwrap();
    let mut previous = HashMap::new();
    let mut stream: HashMap<(u32,u32), Timespan> = HashMap::new();
    let mut count = 0u64;
   //  let mut last = vec![vec![Timespan{beg:0,end:0};39]; 39];

    let mut cur_time = 0i64;
   //  let mut pause = String::new();
    for record in reader.decode() {
      let (s1, point): (String, Record) = record.unwrap();
      let timestamp = strptime(&s1, "%Y-%m-%d %H:%M:%S").ok()
                                                        .expect("Unable to read the date")
                                                        .to_timespec().sec;
      if point.lat.is_some() && point.long.is_some(){
         previous.insert(point.id, Coord{lat: point.lat.unwrap(),
                                             long: point.long.unwrap(),
                                             time: timestamp});
      }
      // println!("Recording for {}",point.id);
      if cur_time != timestamp {
         let adja = proximity(&previous);
         add_activation(timestamp, &adja, & mut stream);
         // println!("Wainting for an input.");
         // io::stdin().read_line(& mut pause);
         cur_time = timestamp;
      }
      count += 1;
      //   println!("{}: {} {} {}", timestamp, point.id, point.long, point.lat);
    }
    clean_stream(& mut stream);
   //  println!("Il y a {} lignes.", count);
}
