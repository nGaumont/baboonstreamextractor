# README #

This script transforms position traces into a link stream.  
It is intended to work with data from [here](https://www.datarepository.movebank.org/handle/10255/move.405).  

### How do I get set up? ###

Make sure you have rust and cargo installed, see [here](https://www.rust-lang.org/).  
Once everything is set up, just type:

```
cargo build
cargo run time_sorted_file
```
If you want to build in release mode add ```--release``` option.  
The input file is assumed to be time sorted, so make sure it is.  

### Customisation ##

This script outputs when 2 baboons are separated by less than ```MIN_DIST``` meter during the last ```SKIP_TIME``` seconds.  
Default values are 1.5 meter and 10 seconds but feel free to change them in the source.  


### Who do I talk to? ###

If you have any problem to run this, send an email to : noe@ngaumont.fr